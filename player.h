#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>

using namespace std;

class Player {

public:				
    Player(Side side);
    ~Player();
    Board *board;
    Side oppSide;
    Side mySide;
    
    struct Node {
		
		Board *posBoard;
		vector <Node> child;
		Node *parent;
		int score;
		bool hasScore;
		Side turn;
		Side me;
		Move *move;
		
		int calcScore(){
			if (this->child.size() != 0)
			{	
				if (this->hasScore)
				{
					return this->score;
				}else{
					if (turn == me)
					{
						this->hasScore = true;
						int bestScore = -100000;
						for (unsigned int i = 0; i <= this->child.size(); i++)
						{
							if (this->child[i].score > bestScore)
							{
								bestScore = this->child[i].score;
							}
						}
						return bestScore;
					}else{
						this->hasScore = true;
						int bestScore = 100000;
						for (unsigned int i = 0; i <= this->child.size(); i++)
						{
							if (this->child[i].score < bestScore)
							{                    
								bestScore = this->child[i].score;
							}
						}
						return bestScore;
					}
				}
			}else{
				
				return this->posBoard->count(me) - this->posBoard->count(turn);
			}
		}
		
		Node (Board *posBoard, Node *parent, bool hasScore, Side me,
				Side turn, Move *move)
		{
			this->posBoard = posBoard->copy();
			this->parent = parent;
			this->hasScore = false;
			this->me = me;
			this->turn = turn;
			this->move = move;
		}

    };
    
    Move *newMove;
	
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *randomMove();
    Move *hMove();
    Move *hMove2();
    Move *hMove3();
    void addBranch(std::vector<Node*> tree, Node *current, int *indexInc,
					vector<int>posCoords,  Side mySide, Side turn);

    //void addBranch(Node *current, int *indexInc, vector<int>posCoords,  Side mySide, Side turn);

    
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
