#include "player.h"
#include <cstdlib>
#include <vector>
#include <map>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	mySide = side;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
	if (side == BLACK)
	{
		oppSide = WHITE;
	}
	else
	{
		oppSide = BLACK;
	}
	
	//Board *board = Board->copy();
	board = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	//check for NULL just in case newMove is never initialized when code
	//runs
	if (newMove != NULL)
	{
		delete newMove;
	}
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    if (opponentsMove != NULL)
	{
		//update board with opponents move
		board->doMove(opponentsMove, oppSide);
		std::cerr << "opp move: "<< opponentsMove->getX()<<" "<<
									opponentsMove->getY()<<std::endl;
	}else{
	std::cerr << "opp has no move"<<std::endl;
	}	
	
	if (board->hasMoves(mySide))
    {
		//Move *move = randomMove();
		Move *move = hMove3();
		board->doMove(move, mySide);
		std::cerr << "returning my move to " << move->getX() << 
										" "<<move->getY() <<std::endl;
		return move;
	}
    
  
	
	return NULL;
}

/**
int getScore(Board posBoard)
{
	int x_new = 0;
	int y_new = 0;
	
	if (posBoard->count(mySide) - posBoard->count(oppSide) > maxVal)
		{
			maxVal = posBoard->count(mySide) - posBoard->count(oppSide);
			x_new = (*it)/8;
			y_new = (*it)%8;
		}
		
		//check each move for best possible board
	
	int map[][8] = {{40, -30, 5, 5, 5, 5, -30, 40},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {40, -30, 5, 5, 5, 5, -30, 40}};
	
}
**/


void Player::addBranch(vector<Node*> tree, Node* current, int *indexInc, 
                       vector<int>posCoords, Side mySide, Side turn)
{
	std::cerr << "in addBranch"<< std::endl;
	for (std::vector<int>::iterator it = posCoords.begin(); 
		it != posCoords.end(); it++)
	{
		std::cerr << "in addBranch: for loop"<< std::endl;

		Board *newBoard = board->copy();
		Move *posMove = new Move((*it)/8, (*it)%8);
		std::cerr <<"adding newMove: "<< *it/8 <<" "<< *it%8 << std::endl;
		
		newBoard->doMove(posMove, mySide);
		
		Node *newNode = new Node(newBoard, current, false, mySide, turn,
									posMove);
										
		tree.push_back(newNode);
		std::cerr << "tree size: "<< tree.size() << std::endl;
		
		*indexInc += 1;
		std::cerr << "indexInc is:"<< *indexInc << std::endl;
		
	}

}

Move *Player::hMove3() 
{
	std::vector <int> posCoords;
	//find which coords are possible from here and store in vector				
	for (int i = 0; i<8; i++)
	{
		for (int j = 0; j<8; j++)
		{
			Move *posMove = new Move(i,j);
			
			if (board->checkMove(posMove, mySide))
			{	
				posCoord
				
				s.push_back(i*8 + j);
				
				std::cerr << i<<" "<< j<<" is pushed back"<<std::endl;
				
			}else{
				//std::cerr << i<<" "<< j<<"not poss"<<std::endl;
			}
			delete posMove;
			
		}
	}
	
	std::cerr <<"size of posCoords: "<< posCoords.size()<<std::endl;
			
	vector<Node*> tree;
	Node *first = new Node(board, NULL, false, mySide, mySide, NULL);
	tree.push_back(first);
	
	int curLayer = 0, maxLayer = 1;
	
	//start calculating nodes from this index in tree
	int start = 0, end = 1;
	Side turn;
	
	while (curLayer <= maxLayer)
	{
		int indexInc = 0;
		
		//whose turn is it?
		if (curLayer % 2 == 0)
		{
			turn = mySide;
		}else{
			turn = oppSide;
		}
		std::cerr<<"-----------------------"<<std::endl;
		
		std::cerr<<"turn:"<< turn << " current Layer:" <<curLayer<< std::endl;
		
		//populate tree with nodes containing possible moves
		for (int i = start; i <= end; i++)
		{
			std::cerr <<"adding Branch for posMove#"<<i<<std::endl;
			addBranch(tree, tree[i], &indexInc, posCoords, mySide, turn);
	
		}
		
		std::cerr <<"done with Branch"<<std::endl;
		std::cerr <<"indexInc = "<< indexInc << std::endl;
			
		//adjust indices so that I go look at children of the new nodes
		//for the next layer
		start = tree.size() - indexInc;
		end = tree.size();
		
		//moving on to next layer
		curLayer += 1;
		std::cerr << "frontier starts and ends: "<< start <<" "<< end << std::endl;
	
	}
	
	//compare scores of each posMove and pick maximum
	int bestScore = -10000000;
	Node *bestMove;
	for (unsigned int i = 1; i<= posCoords.size(); i++)
	{
		if (tree[i]->calcScore() > bestScore)
		{
			bestMove = tree[i];
			bestScore = tree[i]->calcScore();
		}
	}
	
	return bestMove->move;
}



Move *Player::hMove2() 
{
	std::vector <int> posMoves;
						
	for (int i = 0; i<8; i++)
	{
		for (int j = 0; j<8; j++)
		{
			Move *posMove = new Move(i,j);
			
			if (board->checkMove(posMove, mySide))
			{
				posMoves.push_back(i*8 + j);
				std::cerr << i<<" "<< j<<" is pushed back"<<std::endl;
				
			}else{
//				std::cerr << i<<" "<< j<<"not poss"<<std::endl;
			}
			delete posMove;
			
		}
	}
	
	std::cerr << posMoves.size()<<" poss moves"<<std::endl;
	
	//check each move for best possible board
/*	int map[][8] = {{40, -30, 5, 5, 5, 5, -30, 40},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {-30, 1, 1, 1, 1, 1, 1, -30},
			   {40, -30, 5, 5, 5, 5, -30, 40}};
*/	
	int maxVal = -100000;
	int x_new = -1;
	int y_new = -1;

	for (std::vector<int>::iterator it = posMoves.begin(); 
		it != posMoves.end(); it++)
    {
		std::cerr << *it/8 <<" "<< *it%8 << std::endl;
		Board *posBoard = board->copy();
		
		Move *posMove = new Move((*it)/8, (*it)%8);
		posBoard->doMove(posMove, mySide);
		
		std::cerr << posBoard->count(mySide) <<" posVal"<<std::endl;
	
		if (posBoard->count(mySide) - posBoard->count(oppSide) > maxVal)
		{
			
			maxVal = posBoard->count(mySide) - posBoard->count(oppSide);
			x_new = (*it)/8;
			y_new = (*it)%8;
		}
		
	}
	
	if(x_new == -1) {
		return NULL;
	}
	
	newMove = new Move(x_new, y_new);
	std::cerr << "i move to "<< newMove->getX() <<" "<< newMove->getY()<<std::endl;
	return newMove;

}


Move *Player::hMove() 
{
	std::vector <int> posMoves;
						
	for (int i = 0; i<8; i++)
	{
		for (int j = 0; j<8; j++)
		{
			if (board->occupied(i, j)){
				continue;
			}
			
			Move *posMove = new Move(i,j);
			
			if (board->checkMove(posMove, mySide))
			{
				posMoves.push_back(i*8 + j);
				std::cerr << i<<" "<< j<<" is pushed back"<<std::endl;
				
			}else{
				std::cerr << i<<" "<< j<<"not poss"<<std::endl;
			}
			delete posMove;
			
		}
	}
	
	
	std::cerr << posMoves.size()<<" poss moves"<<std::endl;
	
	//map each posMove to a value
	std::map <int, int> valueMap;
	
	for (std::vector<int>::iterator it = posMoves.begin(); 
		it != posMoves.end(); ++it)
    {
		if (*it == 63 || *it == 0 || (*it / 8 == 0 && *it % 8 == 7) || 
			(*it / 8 == 0 && *it % 8 == 7))
		{
			valueMap[*it] = 50;
		}
		else if((*it / 8 == 0) || (*it % 8 == 0) || 
				 (*it / 8 == 7) || (*it % 8 == 7))
		{
			if ((*it / 8 || *it % 8 == 1) || (*it / 8 || *it % 8 == 1))
			{ 
				valueMap[*it] = -5;
			}
			else
			{
				valueMap[*it] = 7;
			}
		}
		else
		{
			valueMap[*it] = 1;
		}
			
	}
	
	int maxVal = -100;
	int x_new = 0;
	int y_new = 0;
 
	//compare values and pick one with highest value
	for (std::map<int,int>::iterator m=valueMap.begin(); m!=valueMap.end(); 
		 m++)
	{
		if (m->second > maxVal)
		{
			maxVal = m->second;
			x_new = m->first / 8;
			y_new = m->first % 8;
		}
	}
  
	

	*newMove = Move(x_new, y_new);
	std::cerr << "i move to "<< newMove->getX() <<" "<< newMove->getY()<<std::endl;
return newMove;

}



Move *Player::randomMove() 
{
	std::vector <int> posMoves;
						
	for (int i = 0; i<8; i++)
	{
		for (int j = 0; j<8; j++)
		{
			if (board->occupied(i, j)){
				continue;
			}
			
			Move *posMove = new Move(i,j);
			
			if (board->checkMove(posMove, mySide))
			{
				posMoves.push_back(i*8 + j);
				std::cerr << i<<" "<< j<<" is pushed back"<<std::endl;
				
			}else{
				std::cerr << i<<" "<< j<<"not poss"<<std::endl;
			}
			delete posMove;
			
		}
	}
	
	std::cerr << posMoves.size()<<" poss moves"<<std::endl;
	
	
	//pick a random move:
	int coord = posMoves[rand()%posMoves.size()];
	int x_new = coord / 8;
	int y_new = coord % 8;

	newMove = new Move(x_new, y_new);
	std::cerr << "i move to "<< newMove->getX() <<" "<< newMove->getY()<<std::endl;
return newMove;

}
