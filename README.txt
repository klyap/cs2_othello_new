I started out using just the simple unweighted heuristic.
I tried making a matrix of weights, making corners worth a lot,
the squares surrounding the corner worth really little, but I couldn't
figure how to implement it.

Next, I made a minimax tree. I made a struct called Node and had a vector of

them called my "tree". Each "layer" of the tree is indexed by "start" and
"end" that change based on the number of new nodes that are added to the
tree. For each possible move, I want to create a new Node for each possible node
after that. I keep doing this for the desired numbe of layers.

Then, I will go through each layer of the tree vector and if the node has no children,
I know that it's the last one, and I want to calculate its score using
calcScore() and set it's "value" property to this score. If a node has 
a score/value, it will return the max/min value, depending on whether 
the move was for mySide or oppSide. This goes on until a node has
no parent (and is thus the root node), and it gets the max score. 
Then I will return the move that got that corresponding score.

The first attempt segfaulted because I had the for-loop going through
the tree without actually putting anything into the tree in the first place.
I fixed this and also made it so that indexInc actually increments by passing
in its address. Then I found another error with the makeBranch function.
It wasn't actually changing the tree vector and I had to make it a pointer or pass in its address,
but I messed up along the way.





PSEUDOCODE
addBranch(current->posBoard)
{
   get vector of posCoords

   //storeCoords in tree
for each posCoord:
make new node
node->parent = current
node->posBoard = possCoord-Moved board
//node->value = calcVal(posBoard)
current->child.pushback(node)
pushback node onto tree
frontierInc += 1;
delete node
}

tree = vector of Nodes

-----------


//populate tree
addBranch(board)       // aka pushback current board-ed node onto tree

curLayer = 0; wantedLayer = 3;
start = 0; end = 1;
while curLayer <= wantedLayer{
   
   //for each tree-node in frontier
   each = from tree[frontierIndex] to tree.end
   {
	current = each
	addBranch(posCoords, current)    //adds a set of posBoards to new frontier
   }

   curLayer += 1;
   start += frontierInc
   end = ????
}

//do minimax

calcScore(Node cur){
if have children:
  if score != NULL
    retun score
  else
    if my turn: return max children's scores
    else: return min children's scores
else:
  return board.getscore
}

for each posMove (aka 1st layer of tree)
    if each->calcScore > bestScore:
	bestMove = each;
	bestScore = each->calcScore;
    